#!/usr/bin/env python3
'''
Base 64 image decoder taken from this link
https://www.geeksforgeeks.org/python-convert-image-to-string-and-vice-versa/
'''

import base64

'''
Command explanation 
open('file.png', rb)
- The rb is a args for the open command - r - read , b = byte 
'''

file = open('img.bin', 'rb')
byte = file.read()
file.close()

'''
Command explanation 
open('file.png',wb)
- The rb is a args for the open command 
    - w - write , 
      b = bytes 
'''


decodeit = open('img.png', 'wb')
decodeit.write(base64.b64decode((byte)))
decodeit.close()
